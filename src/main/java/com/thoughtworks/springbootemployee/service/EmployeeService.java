package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.constant.OperationResultType;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> getAllEmployee() {
        return employeeRepository.getAllEmployee();
    }

    public String addEmployee(Employee employee) {
        employee.setId(generateId());
        if (employeeRepository.addEmployee(employee)) {
            return OperationResultType.SUCCESS;
        }
        return OperationResultType.FAIL;
    }

    public Employee getEmployeeById(Long id) {
        return employeeRepository.getEmployeeById(id);
    }

    public List<Employee> getEmployeesByGender(String gender) {
        return employeeRepository.getEmployeesByGender(gender);
    }

    public String updateEmployeeInfo(Long id, Employee employee) {
        if (employee.getId() == null || !employeeRepository.updateEmployeeInfo(id, employee)) {
            return OperationResultType.FAIL;
        }
        return OperationResultType.SUCCESS;
    }

    public String deleteEmployeeById(Long id) {
        if (employeeRepository.deleteEmployeeById(id)){
            return OperationResultType.SUCCESS;
        }
        return OperationResultType.FAIL;
    }

    public List<Employee> queryEmployeeByPage(Integer page, Integer size) {
        return employeeRepository.queryEmployeeByPage(page, size);
    }

    public List<Employee> findEmployeeByCompanyId(Long id) {
        return employeeRepository.getAllEmployee()
                .stream()
                .filter(employee -> employee.getCompanyId().equals(id))
                .collect(Collectors.toList());
    }

    private Long generateId() {
        long maxId = employeeRepository.getAllEmployee().stream().mapToLong(Employee::getId).max().orElse(0L);
        return maxId + 1;
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return employeeRepository.findEmployeesByCompanyId(id);
    }
}
