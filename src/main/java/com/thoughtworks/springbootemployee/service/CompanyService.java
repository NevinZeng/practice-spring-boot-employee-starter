package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.constant.OperationResultType;
import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EmployeeService employeeService;

    public List<Company> getAllCompany() {
        return companyRepository.getAllCompany();
    }

    public Company findCompanyById(Long id) {
        return companyRepository.findCompanyById(id);
    }

    public List<Employee> findEmployeeByCompanyId(Long companyId) {
        return employeeService.findEmployeeByCompanyId(companyId);
    }

    public List<Company> queryCompanyByPage(Integer page, Integer size) {
        return companyRepository.queryCompanyByPage(page, size);
    }

    public String addCompany(Company company) {
        company.setId(generateId());
        if (companyRepository.addCompany(company)){
            return OperationResultType.SUCCESS;
        }
        return OperationResultType.FAIL;
    }

    public String updateCompany(Long id, Company company) {
        if (company.getId() == null || !companyRepository.updateCompany(id, company)){
            return OperationResultType.FAIL;
        }
        return OperationResultType.SUCCESS;
    }

    public String deleteCompanyById(Long id) {
        if (companyRepository.deleteCompanyById(id)){
            employeeService.findEmployeesByCompanyId(id)
                    .forEach(employee -> employeeService.deleteEmployeeById(employee.getId()));
            return OperationResultType.SUCCESS;
        }
        return OperationResultType.FAIL;
    }

    private Long generateId() {
        long maxId = companyRepository.getAllCompany()
                .stream()
                .mapToLong(Company::getId)
                .max()
                .orElse(0L);
        return maxId + 1;
    }
}
