package com.thoughtworks.springbootemployee.constant;

public class OperationResultType {

    public final static String SUCCESS = "success";
    public final static String FAIL =  "fail";
}
