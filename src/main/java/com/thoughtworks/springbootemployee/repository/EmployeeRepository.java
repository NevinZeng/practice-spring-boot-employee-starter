package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    private final static List<Employee> employeeList = new ArrayList<>();

    public List<Employee> getAllEmployee() {
        return employeeList;
    }

    public Boolean addEmployee(Employee employee) {
        return employeeList.add(employee);
    }

    public Employee getEmployeeById(Long id) {
        return employeeList.stream().filter(employee -> employee.getId().equals(id)).findFirst().orElse(null);
    }

    public List<Employee> getEmployeesByGender(String gender) {
        return employeeList.stream().filter(employee -> employee.getGender().equals(gender)).collect(Collectors.toList());
    }

    public boolean updateEmployeeInfo(Long id, Employee employee) {
        Employee willUpdateEmployee = employeeList.stream()
                .filter(employeeItem -> employeeItem.getId().equals(id))
                .findFirst()
                .orElse(null);
        if (willUpdateEmployee == null){
            return false;
        }
        willUpdateEmployee.setAge(employee.getAge());
        willUpdateEmployee.setSalary(employee.getSalary());
        return true;
    }

    public boolean deleteEmployeeById(Long id) {
        return employeeList.removeIf(employee -> employee.getId().equals(id));
    }

    public List<Employee> queryEmployeeByPage(Integer page, Integer size) {
        return employeeList.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> findEmployeesByCompanyId(Long companyId) {
        return employeeList.stream().filter(employee -> employee.getCompanyId().equals(companyId)).collect(Collectors.toList());
    }
}
