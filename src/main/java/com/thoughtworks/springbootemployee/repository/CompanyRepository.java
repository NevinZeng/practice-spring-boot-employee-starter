package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {

    private final List<Company> companyList = new ArrayList<>();


    public List<Company> getAllCompany() {
        return companyList;
    }

    public Company findCompanyById(Long id) {
        return companyList.stream().filter(companyItem -> companyItem.getId().equals(id)).findFirst().orElse(null);
    }

    public List<Company> queryCompanyByPage(Integer page, Integer size) {
        return companyList.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public boolean addCompany(Company company) {
        return companyList.add(company);
    }

    public boolean updateCompany(Long id, Company inputCompany) {
        Company willUpdateCompany = companyList.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);
        if (willUpdateCompany == null){
            return false;
        }
        willUpdateCompany.setName(inputCompany.getName());
        return true;
    }

    public boolean deleteCompanyById(Long id) {
        return companyList.removeIf(company -> company.getId().equals(id));
    }
}
