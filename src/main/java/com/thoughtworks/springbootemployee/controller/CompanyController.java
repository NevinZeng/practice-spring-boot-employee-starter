package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    private CompanyService companyService;


    @GetMapping
    public List<Company> getAllCompany(){
        return companyService.getAllCompany();
    }

    @GetMapping("/{id}")
    public Company findCompanyById(@PathVariable Long id){
        return companyService.findCompanyById(id);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> findEmployeeByCompanyId(@PathVariable Long companyId){
        return companyService.findEmployeeByCompanyId(companyId);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> queryCompanyByPage(@RequestParam Integer page, @RequestParam Integer size){
        return companyService.queryCompanyByPage(page, size);
    }

    @PostMapping
    public String addCompany(@RequestBody Company company){
        return companyService.addCompany(company);
    }

    @PutMapping("/{id}")
    public String updateCompany(@PathVariable Long id, @RequestBody Company company){
        return companyService.updateCompany(id, company);
    }

    @DeleteMapping("/{id}")
    public String deleteCompanyById(@PathVariable Long id){
        return companyService.deleteCompanyById(id);
    }
}
