## O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

- Code Review and Show Case.

- The Context Map of Refactoring was described. Like day 1, the group discussed and selected representatives to share.

- Learn Http basics, including the structure of URIs, and more.

- Learning Restful

- Learn about Pair Programing and try programming exercises with Pair Programing.

- Learn SpringBoot, including what SpringBoot is and how to use it.

## R (Reflective): Please use one word to express your feelings about today's class.

   Progressive.

## I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

- Understanding the fundamentals and concepts of HTTP is important for Web development. HTTP is the protocol used to transfer data between clients and servers, and understanding how it works and the common request methods (GET, POST, etc.) helps me build reliable and efficient Web services.

- Learn Spring Boot, including what Spring Boot is and how to use it. Spring Boot is a framework for simplifying Java Web application development. It provides a quick and easy way to configure and deploy applications, and offers a variety of features and plug-ins to speed up development. Learning Spring Boot helps me build efficient, scalable Web applications and improve development efficiency.

## D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

- In the software development process, it is important to have a solid Http foundation. I will continue to learn more about Http knowledge, deepen the impression.
